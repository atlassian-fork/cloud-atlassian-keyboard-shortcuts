package com.atlassian.plugins.shortcuts.internal;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcut;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutModuleDescriptor;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutOperation;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.util.concurrent.ResettableLazyReference;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * TODO: Document this class / interface here
 *
 * @since v4.0
 */
public class ShortcutsResettableLazyReference extends ResettableLazyReference<List<KeyboardShortcut>>
{
    private final PluginAccessor pluginAccessor;
    private HttpContext httpContext;

    public ShortcutsResettableLazyReference(final PluginAccessor pluginAccessor, HttpContext httpContext)
    {
        this.pluginAccessor = pluginAccessor;
        this.httpContext = httpContext;
    }

    @Override
    protected List<KeyboardShortcut> create() throws Exception
    {
        final List<KeyboardShortcut> shortcuts = new ArrayList<KeyboardShortcut>();
        final List<KeyboardShortcutModuleDescriptor> descriptors =
                pluginAccessor.getEnabledModuleDescriptorsByClass(KeyboardShortcutModuleDescriptor.class);
        HttpServletRequest request = httpContext.getRequest();
        for (KeyboardShortcutModuleDescriptor descriptor : descriptors)
        {
            //We want to prepend the context path for goTo shortcuts
            KeyboardShortcut original = descriptor.getModule();
            KeyboardShortcut.Builder shortcutBuilder = KeyboardShortcut.builder(original);
            KeyboardShortcutOperation operation = original.getOperation();
            if(operation.getType() == KeyboardShortcutOperation.OperationType.goTo)
            {
                URI uri = new URI(operation.getParam());
                if(!uri.isAbsolute() && !uri.isOpaque() && request != null){
                    shortcutBuilder.setOperationParam(request.getContextPath() + uri.toString());
                }
            }
            shortcuts.add(shortcutBuilder.build());
        }
        Collections.sort(shortcuts);
        return shortcuts;
    }
}
