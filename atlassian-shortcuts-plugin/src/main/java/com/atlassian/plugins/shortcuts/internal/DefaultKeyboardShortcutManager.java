package com.atlassian.plugins.shortcuts.internal;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcut;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutManager;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutModuleDescriptor;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.util.concurrent.ResettableLazyReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class DefaultKeyboardShortcutManager implements KeyboardShortcutManager
{
    private static final Logger log = LoggerFactory.getLogger(DefaultKeyboardShortcutManager.class);
    private String applicationContextPath;
    private HttpContext httpContext;

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    /**
     * To speed up performance, we sort the shortcuts in a lazyreference which will only get reset if a keyboard
     * shortcut is registered/unregistered.
     */
    private final ResettableLazyReference<List<KeyboardShortcut>> ref;

    /**
     * Generating the md5 hash for keyboard shortcuts can also be expensive so in order to make this as performant as
     * possible, we use a resettable lazy reference to ensure we only have to calculate it after a keyboard shortcut was
     * registered/unregistered.
     */
    private final ResettableLazyReference<String> hashRef = new ResettableLazyReference<String>()
    {
        @Override
        protected String create() throws Exception
        {
            List<KeyboardShortcut> shortcuts = getAllShortcuts();
            return Hasher.getHash(shortcuts);
        }
    };

    public DefaultKeyboardShortcutManager(final PluginAccessor pluginAccessor, final PluginEventManager pluginEventManager, HttpContext httpContext)
    {
        //TODO - ues KeyboardShortcutContextProvider to determine if keyboardShortcuts are enabled.
        //See AKS-15
        this.applicationContextPath = "";
        this.ref = new ShortcutsResettableLazyReference(pluginAccessor, httpContext);
        this.httpContext = httpContext;
        pluginEventManager.register(this);
    }

    @PluginEventListener
    public void handleEvent(Object event)
    {
        ModuleDescriptor<?> moduleDescriptor;
        
        if(event instanceof PluginModuleDisabledEvent)
            moduleDescriptor = ((PluginModuleDisabledEvent)event).getModule();
        else if(event instanceof PluginModuleEnabledEvent)
            moduleDescriptor = ((PluginModuleEnabledEvent)event).getModule();
        else
            return;
        
        if (!KeyboardShortcutModuleDescriptor.class.isInstance(moduleDescriptor))
            return;

        log.info("KeyboardShortcutModuleDescriptor plugin module event detected - resetting references");
        lock.writeLock().lock();
        try
        {
            ref.reset();
            hashRef.reset();
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public String getShortcutsHash()
    {
        lock.readLock().lock();
        try
        {
            return hashRef.get();
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    public List<KeyboardShortcut> getAllShortcuts()
    {
        lock.readLock().lock();
        HttpServletRequest request = httpContext.getRequest();
        if(request != null && !request.getContextPath().equals(applicationContextPath))
        {
            //the context path has changed, we should rebuild shortcuts
            this.ref.reset();
            hashRef.reset();
            this.applicationContextPath = request.getContextPath();
        }
        try
        {
            return ref.get();
        }
        finally
        {
            lock.readLock().unlock();
        }
    }
}
