package com.atlassian.plugins.shortcuts.internal;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutManager;

import java.util.Map;

/**
 * Adds the KeyboardShortcutManager to the Velocity context for rendering
 * {@link WebPanel}s.
 */
public class KeyboardShortcutManagerProvider implements ContextProvider
{
    private KeyboardShortcutManager keyboardShortcutManager;

    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {
    }

    public Map<String, Object> getContextMap(Map<String, Object> stringObjectMap)
    {
        stringObjectMap.put("keyboardShortcutManager", keyboardShortcutManager);
        return stringObjectMap;
    }

    public void setKeyboardShortcutManager(KeyboardShortcutManager keyboardShortcutManager)
    {
        this.keyboardShortcutManager = keyboardShortcutManager;
    }
}
