package com.atlassian.plugins.shortcuts.internal;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcut;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutManager;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutModuleDescriptor;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutOperation;
import com.atlassian.sal.api.web.context.HttpContext;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultKeyboardShortcutManager extends TestCase
{
    KeyboardShortcutManager shortcutManager;
    PluginAccessor mockPluginAccesor;


    public void setUp() throws Exception
    {
        mockPluginAccesor = mock(PluginAccessor.class);
        final PluginEventManager mockEventManger = mock(PluginEventManager.class);
        final HttpContext mockHttpContext = mock(HttpContext.class);
        final String mockBaseUrl = "/confluence";
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        shortcutManager = new DefaultKeyboardShortcutManager(mockPluginAccesor, mockEventManger, mockHttpContext);
        when(mockHttpContext.getRequest()).thenReturn(mockRequest);
        when(mockRequest.getContextPath()).thenReturn(mockBaseUrl);
    }

    public void testNoShortcuts()
    {
        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(KeyboardShortcutModuleDescriptor.class)).thenReturn(new ArrayList());
        Collection allShortcuts = shortcutManager.getAllShortcuts();
        assertEquals(0, allShortcuts.size());
    }

    public void testAddShortcut()
    {
        final KeyboardShortcut keyboardShortcut = addKeyboardShortcut();

        List<KeyboardShortcut> allShortcuts = shortcutManager.getAllShortcuts();
        assertEquals(1, allShortcuts.size());
        assertEquals(keyboardShortcut, allShortcuts.get(0));
    }

    public void testGetHash() throws Exception
    {
        addKeyboardShortcut();

        String hash1 = shortcutManager.getShortcutsHash();
        assertTrue(StringUtils.isNotBlank(hash1));

        // Check that a second call returns the same hash
        String hash2 = shortcutManager.getShortcutsHash();
        assertEquals(hash1, hash2);
    }

    private KeyboardShortcut addKeyboardShortcut()
    {
        Set<List<String>> keys = new HashSet<List<String>>();
        keys.add(Arrays.asList("g", "h"));
        KeyboardShortcutOperation operation = new KeyboardShortcutOperation("followLink", "#test");
        final KeyboardShortcut keyboardShortcut = new KeyboardShortcut("global", operation, 0, keys, "blah", "blah", false);
        KeyboardShortcutModuleDescriptor moduleDescriptor = new KeyboardShortcutModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY) {
            public KeyboardShortcut getModule() {
                return keyboardShortcut;
            }
        };
        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(KeyboardShortcutModuleDescriptor.class)).thenReturn(
            Collections.<KeyboardShortcutModuleDescriptor>singletonList(moduleDescriptor));
        return keyboardShortcut;
    }
}
