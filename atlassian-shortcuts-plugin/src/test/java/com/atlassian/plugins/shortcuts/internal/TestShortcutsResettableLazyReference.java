package com.atlassian.plugins.shortcuts.internal;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcut;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutModuleDescriptor;
import com.atlassian.plugins.shortcuts.api.KeyboardShortcutOperation;
import com.atlassian.sal.api.web.context.HttpContext;
import junit.framework.TestCase;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * TODO: Document this class / interface here
 *
 * @since v4.0
 */
public class TestShortcutsResettableLazyReference extends TestCase
{
    public void testGetSortedShortcuts()
    {
        final PluginAccessor mockPluginAccesor = mock(PluginAccessor.class);
        final HttpContext mockHttpContext = mock(HttpContext.class);
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        final Set<List<String>> shortcuts = new HashSet<List<String>>();
        final String mockBaseUrl = "/confluence";
        List<String> shortcut = new ArrayList<String>();
        shortcut.add("ab");
        shortcuts.add(shortcut);
        KeyboardShortcutOperation operation = new KeyboardShortcutOperation("click", "#stuff");
        final List<KeyboardShortcutModuleDescriptor> descriptors = new ArrayList<KeyboardShortcutModuleDescriptor>();
        descriptors.add(new MockKeyboardShortcutModuleDescriptor(new KeyboardShortcut("global", operation, 45, shortcuts, "i18nkey", "default", false)));
        descriptors.add(new MockKeyboardShortcutModuleDescriptor(new KeyboardShortcut("dude", operation, 15, shortcuts, "i18nkey", "default", false)));
        descriptors.add(new MockKeyboardShortcutModuleDescriptor(new KeyboardShortcut("issue", operation, 32, shortcuts, "i18nkey", "default", false)));
        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(KeyboardShortcutModuleDescriptor.class)).thenReturn(descriptors);

        when(mockHttpContext.getRequest()).thenReturn(mockRequest);
        when(mockRequest.getContextPath()).thenReturn(mockBaseUrl);

        final ShortcutsResettableLazyReference ref = new ShortcutsResettableLazyReference(mockPluginAccesor, mockHttpContext);
        final List<KeyboardShortcut> allShortcuts = ref.get();

        assertEquals(3, allShortcuts.size());
        assertEquals("dude", allShortcuts.get(0).getContext());
        assertEquals("issue", allShortcuts.get(1).getContext());
        assertEquals("global", allShortcuts.get(2).getContext());
    }

    static class MockKeyboardShortcutModuleDescriptor extends KeyboardShortcutModuleDescriptor
    {
        private final KeyboardShortcut shortcut;

        public MockKeyboardShortcutModuleDescriptor(KeyboardShortcut shortcut)
        {
            super(ModuleFactory.LEGACY_MODULE_FACTORY);
            this.shortcut = shortcut;
        }

        @Override
        public KeyboardShortcut getModule()
        {
            return shortcut;
        }
    }

}
