package com.atlassian.plugins.shortcuts.api;

import java.util.List;

/**
 * Provides a registry of keyboard shortcuts currently available.  Keyboard shortcuts can be provided via the
 * {@link KeyboardShortcutModuleDescriptor} plugin point.
 *
 * @since v4.1
 */
public interface KeyboardShortcutManager
{
    /**
     * Returns an ordered list of all {@link KeyboardShortcut}s available using the
     * plugin descriptor's 'order' attribute for sorting.  Implementations should take care to implement this method as
     * quickly as possible since it will be called very often.
     *
     * @return ordered list of all registered keyboard shortcuts
     */
    List<KeyboardShortcut> getAllShortcuts();

    /**
     * Defines the default global context for keyboard shortcuts that apply everywhere.
     */
    String CONTEXT_GLOBAL = "global".intern();

    /**
     * Returns a hash of the list provided by {@link #getAllShortcuts()}.
     *
     * @return a hash of the currently-available KeyboardShortcuts.
     */
    String getShortcutsHash();
}
