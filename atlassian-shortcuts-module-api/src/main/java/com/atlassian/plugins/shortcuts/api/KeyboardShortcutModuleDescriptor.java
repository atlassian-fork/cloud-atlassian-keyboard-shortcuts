package com.atlassian.plugins.shortcuts.api;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Provides a plugin point to define keyboard shortcuts.  Shortcuts are made up of a set of shortcut keys for this
 * particular operation, the type of operation, a context in which the operation applies and a parameter for the
 * operation.  They are also ordered, which defines in which order they will appear on the help screen.
 *
 * @since v4.1
 */
public class KeyboardShortcutModuleDescriptor extends AbstractModuleDescriptor<KeyboardShortcut>
{
    public static final String XML_ELEMENT_NAME = "keyboard-shortcut".intern();

    private static final Logger log = LoggerFactory.getLogger(KeyboardShortcutModuleDescriptor.class);

    private static final Pattern JSON_VALUE_STRING = Pattern.compile("^\".*\"$");
    private static final Pattern JSON_VALUE_ARRAY = Pattern.compile("^\\[.*\\]$");
    private KeyboardShortcut keyboardShortcut;

    public KeyboardShortcutModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException
    {
        super.init(plugin, element);

        String descriptionKey = parseDescriptionKey(element);
        String defaultDescription = parseDefaultDescription(element);
        boolean hidden = parseHidden(element);
        String context = parseContext(element);
        Set<List<String>> shortcuts = parseShortcuts(element);
        int order = parseOrder(element);
        KeyboardShortcutOperation operation = parseOperation(element);

        keyboardShortcut = new KeyboardShortcut(context, operation, order, shortcuts, descriptionKey, defaultDescription, hidden);
    }

    private String parseDescriptionKey(Element element)
    {
        final Element descriptionEl = element.element("description");
        if (descriptionEl != null && descriptionEl.attribute("key") != null)
        {
            return StringUtils.trim(descriptionEl.attributeValue("key"));
        }
        else
        {
            throw new PluginParseException("<description> i18n 'key' attribute is a required attribute for a keyboard shortcut plugin module");
        }
    }

    private String parseDefaultDescription(Element element)
    {
        final Element descriptionEl = element.element("description");
        if (descriptionEl != null && !StringUtils.isBlank(descriptionEl.getText()))
        {
            return StringUtils.trim(descriptionEl.getText());
        }
        return null;
    }

    private Set<List<String>> parseShortcuts(Element element) throws PluginParseException
    {
        Set<List<String>> shortcuts = new HashSet<List<String>>();
        final List<Element> shortcutEls = element.elements("shortcut");

        if (shortcutEls.size() <= 0)
            throw new PluginParseException("<shortcut> is a required element for a keyboard shortcut plugin module");

        for(Element shortcutEl : shortcutEls)
        {
            shortcuts.add(parseShortcut(shortcutEl.getTextTrim()));
        }
        return shortcuts;
    }

    private String parseContext(Element element)
    {
        String context = KeyboardShortcutManager.CONTEXT_GLOBAL;
        final Element contextEl = element.element("context");
        if (contextEl != null)
        {
            context = StringUtils.trim(contextEl.getText());
        }
        return context;
    }

    private boolean parseHidden(Element element)
    {
        final Attribute hiddenAttr = element.attribute("hidden");
        if (hiddenAttr != null && StringUtils.isNotEmpty(hiddenAttr.getText()))
        {
            return Boolean.parseBoolean(hiddenAttr.getText());
        }
        else
        return false;
    }

    private int parseOrder(final Element element)
    {
        //default to the integer max value if no order was specified.
        if (element.element("order") == null)
            return Integer.MAX_VALUE;

        String orderString = element.element("order").getTextTrim();
        if (StringUtils.isBlank(orderString))
        {
            throw new PluginParseException("Invalid order element: cannot be empty");
        }
        try
        {
            return Integer.parseInt(orderString);
        }
        catch (NumberFormatException e)
        {
            log.warn("Invalid order specified: " + element.element("order").getTextTrim() + ". Should be an integer.", e);
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public KeyboardShortcut getModule()
    {
        return keyboardShortcut;
    }

    public int getOrder()
    {
        return keyboardShortcut.getOrder();
    }

    public boolean isHidden()
    {
        return keyboardShortcut.isHidden();
    }

    private KeyboardShortcutOperation parseOperation(Element element) {
        final Element operationEl = element.element("operation");
        if (operationEl == null)
        {
            throw new PluginParseException("<operation> is a required element for a keyboard shortcut plugin module");
        }

        String operationType = operationEl.attribute("type").getText();
        String operationParam = StringUtils.trim(operationEl.getText());
        return new KeyboardShortcutOperation(operationType, operationParam);
    }

    private static List<String> parseShortcut(String shortcut) throws PluginParseException
    {
        List<String> result = new ArrayList<String>();

        try
        {
            if (JSON_VALUE_ARRAY.matcher(shortcut).matches())
            {
                JSONArray array = new JSONArray(shortcut);
                for (int i = 0; i < array.length(); i++)
                {
                    result.add(array.getString(i));
                }
            }
            else if (JSON_VALUE_STRING.matcher(shortcut).matches())
            {
                final String key = "shortcut";
                JSONObject json = new JSONObject(String.format("{ \"%s\": %s }", key, shortcut));
                result = Arrays.asList(json.getString(key));
            }
            else
            {
                for (char c : shortcut.toCharArray())
                {
                    result.add(String.valueOf(c));
                }
            }
        }
        catch (JSONException e)
        {
            throw new PluginParseException("The <shortcut> element did not provide a valid keyboard shortcut definition");
        }

        if (result.size() <= 0)
            throw new PluginParseException("The <shortcut> element did not provide a keyboard shortcut definition");

        return result;
    }

}
