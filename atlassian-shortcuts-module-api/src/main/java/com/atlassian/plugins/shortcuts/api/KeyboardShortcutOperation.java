package com.atlassian.plugins.shortcuts.api;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.util.Assertions;

/**
 * Defines the different operations that can be taken using a {@link KeyboardShortcut}.
 * Operations are always coupled with a parameter which will generally be a jQuery selector or URL.  The parameter
 * can however also be more generic when coupled with the 'execute' operation where a generic javascript function
 * can be specified as well.
 */
public class KeyboardShortcutOperation
{
    private final OperationType type;
    private final String param;

    public KeyboardShortcutOperation(String type, String param) {
        try
        {
            this.type = OperationType.valueOf(type);
        }
        catch (IllegalArgumentException e)
        {
            throw new PluginParseException("Invalid operation type '" + type + "' provided");
        }
        this.param =  Assertions.notNull("param",param);
    }

    public OperationType getType() {
        return type;
    }

    public String getParam() {
        return param;
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof KeyboardShortcutOperation)
        {
            KeyboardShortcutOperation otherShortcut = (KeyboardShortcutOperation) other;
            if(otherShortcut.getType() == this.getType() && otherShortcut.getParam().equals(this.getParam()))
                return true;
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return (43 * (37 + this.getParam().hashCode()) + this.getType().hashCode());
    }

    public static enum OperationType
    {
        /**
         * Clicks an element identified by a jQuery selector
         */
        click,
        /**
         * Changes the window.location to go to a specified location
         */
        goTo,
        /**
         * Sets the window.location to the href value of the link specified by the jQuery selector.
         */
        followLink,
        /**
         * Scrolls the window to a specific element and focuses that element using a jQuery selector
         */
        moveToAndFocus,

        /**
         * Scrolls the window to an element and clicks that elment using a jQuery selector
         */
        moveToAndClick,

        /**
         * Scrolls to and adds <em>focused</em> class to the next item in the jQuery collection
         */
        moveToNextItem,

        /**
         * Scrolls to and adds <em>focused</em> class to the previous item in the jQuery collection
         */
        moveToPrevItem,

        /**
         * Executes a javascript functions specified by the operation parameter
         */
        execute,

        /**
         * Evaluates javascript on page load. Execute above will execute the javascript provided when the keyboard
         * shortcut is pressed.
         */
        evaluate
    }
}
